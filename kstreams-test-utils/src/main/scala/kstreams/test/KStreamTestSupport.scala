package kstreams.test

/**
 * This trait aims to ease integration tests of Kafka-related applications. It contains :
 * - a [[KafkaEnvConf]] that retrieves configuration from the support/docker.conf file. It uses configuration from kafkaEnvConf.
 * - a [[ProducerSupport]] with facility methods to produce records
 * - an [[AdminClientSupport]] with facility methods to make operations on Kafka cluster
 * - a [[ConsumerSupport]] with facility methods to consume in a Kafka topic. It uses configuration from kafkaEnvConf.
 */
trait KStreamTestSupport {
  lazy val kafkaEnvConf: KafkaEnvConf.type = KafkaEnvConf
  lazy val producer: ProducerSupport.type = ProducerSupport
  lazy val adminClient: AdminClientSupport.type = AdminClientSupport
  lazy val consumer: ConsumerSupport.type = ConsumerSupport
}
