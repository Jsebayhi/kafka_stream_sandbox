package kstreams.test

import org.apache.kafka.common.header.Headers

import java.net.ServerSocket
import java.nio.charset.StandardCharsets

object Utils {
  def randomId: String = {
    java.util.UUID.randomUUID().toString
  }

  def randomTimeID: String = {
    System.currentTimeMillis().toString
  }

  def stringToByteArray(str: String): Array[Byte] = {
    str.getBytes(StandardCharsets.UTF_8)
  }

  def arrayByteToString(bytes: Array[Byte]): String = {
    new String(bytes, StandardCharsets.UTF_8)
  }

  def stringHeader(h: Headers): Map[String, Array[String]] = {
    h.toArray
      .groupBy(_.key())
      .map {
        case (k, hList) => (k, hList.map(h => arrayByteToString(h.value())))
      }
  }

  def getAvailablePort(): Int = {
    val r = new ServerSocket(0)
    val port = r.getLocalPort
    r.close()
    port
  }
}
