package kstreams.test

import org.apache.kafka.clients.admin.{ AdminClient, AdminClientConfig }

/**
 * This object gives helper methods to create an AdminClient. It uses configuration from [[KafkaEnvConf]] object.
 *
 * The doAndClose method create an AdminClient, realizes the given operation, and close the client.
 */
object AdminClientSupport {
  import Implicits._
  lazy val conf: Map[String, AnyRef] = Map(
    AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG -> KafkaEnvConf.kafka.url,
    AdminClientConfig.CLIENT_ID_CONFIG -> Utils.randomTimeID
  )

  def createAdminClient(): AdminClient = {
    AdminClient.create(conf)
  }

  def doAndClose[T](f: AdminClient => T): T = {
    val adm = createAdminClient()
    try {
      f(adm)
    } finally {
      adm.close()
    }
  }
}
