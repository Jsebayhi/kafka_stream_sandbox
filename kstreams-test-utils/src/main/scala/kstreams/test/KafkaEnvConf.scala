package kstreams.test

import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

/**
 * This object retrieves Kafka related configuration from a support/docker.conf file.
 */
object KafkaEnvConf {
  private lazy val logger = LoggerFactory.getLogger(this.getClass)

  private lazy val config = ConfigFactory.load("support/docker.conf")

  lazy val kafka = KafkaConf(
    host = config.getString("kafka.host"),
    port = config.getInt("kafka.port")
  )
  lazy val schemaRegistry = KafkaConf(
    host = config.getString("schema-registry.host"),
    port = config.getInt("schema-registry.port")
  )

  logger.debug(
    s"""loaded config:
       |kafka: ${kafka}
       |schemaRegistry: ${schemaRegistry}
       |""".stripMargin)
}

case class KafkaConf(host: String, port: Int) {
  def url: String = s"$host:$port"

  override def toString: String = {
    s"{host: $host, port: $port}"
  }
}