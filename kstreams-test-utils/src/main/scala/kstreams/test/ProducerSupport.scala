package kstreams.test

import org.apache.kafka.clients.producer.{ KafkaProducer, ProducerConfig, ProducerRecord, RecordMetadata }
import org.apache.kafka.common.serialization.{ ByteArraySerializer, Serializer }
import org.apache.kafka.streams.KeyValue

import java.util.concurrent.CountDownLatch

/**
 * This object gives helper methods to produce messages in a Kafka topic. It uses configuration from [[KafkaEnvConf]] object.
 */
object ProducerSupport {
  import Implicits._
  def producerConf(id: String = Utils.randomTimeID): Map[String, AnyRef] = Map(
    ProducerConfig.BOOTSTRAP_SERVERS_CONFIG -> KafkaEnvConf.kafka.url,
    ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG -> classOf[ByteArraySerializer],
    ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG -> classOf[ByteArraySerializer],
    ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG -> Boolean.box(true),
    ProducerConfig.CLIENT_ID_CONFIG -> s"client-$id"
  )

  def produceRecords[K, V](
    topic: String,
    records: Seq[KeyValue[K, V]],
    keySerializer: Serializer[K],
    valueSerializer: Serializer[V]): Unit = {
    produceRecords[K, V](
      records = records.map(record => new ProducerRecord[K, V](topic, record.key, record.value)),
      keySerializer = keySerializer,
      valueSerializer = valueSerializer
    )
  }

  def produceRecords[K, V](
    records: Seq[ProducerRecord[K, V]],
    keySerializer: Serializer[K],
    valueSerializer: Serializer[V]): Unit = {
    val producer = new KafkaProducer[K, V](producerConf(), keySerializer, valueSerializer)
    try {
      produceRecords[K, V](producer, records)
    } finally producer.close()
  }

  def produceRecords[K, V](
    producer: KafkaProducer[K, V],
    records: Seq[ProducerRecord[K, V]]): Unit = {
    val latch = new CountDownLatch(records.size)
    records.map { record =>
      producer.send(record, (_: RecordMetadata, _: Exception) => {
        latch.countDown()
      })
    }
    producer.flush()
    latch.await()
  }
}
