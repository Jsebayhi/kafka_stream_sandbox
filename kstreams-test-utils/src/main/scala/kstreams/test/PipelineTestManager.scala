package kstreams.test

import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.KafkaStreams.State
import org.apache.kafka.streams.scala.StreamsBuilder
import org.slf4j.LoggerFactory

/**
 * This object aims to ease integration test of a KafkaStreams applications.
 * Given a topology, a topology-related configuration and Kafka related configuration, it creates a wrapped KafkaStreams app.
 * a stateListener is added to the application to log the state transitions
 */
object PipelineTestManager {
  private lazy val logger = LoggerFactory.getLogger(this.getClass)
  import Implicits._

  def apply[T](
    conf: T,
    kStreamConf: Map[String, AnyRef],
    builder: T => StreamsBuilder): PipelineTestManager = {

    val topology = builder(conf).build(kStreamConf)
    logger.debug(s"topology: ${topology.describe()}")
    val stream = new KafkaStreams(topology, mapToProperties(kStreamConf))
    new PipelineTestManager(stream)
  }
}

class PipelineTestManager(val kStreams: KafkaStreams) extends AutoCloseable {

  private lazy val logger = LoggerFactory.getLogger(this.getClass)

  kStreams.setStateListener { (newState: KafkaStreams.State, oldState: KafkaStreams.State) =>
    logger.debug(s"State transitioned from ${oldState} to ${newState}")
  }

  def isReady: Boolean = {
    kStreams.state() == State.RUNNING
  }

  def start(): Unit = {
    logger.debug("Starting pipeline service")
    kStreams.start()
    logger.debug("Pipeline service started")
  }

  override def close(): Unit = {
    kStreams.close()
    logger.debug("Stream stopped")
  }
}