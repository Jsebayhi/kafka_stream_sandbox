package kstreams.test

import org.apache.kafka.clients.admin.CreateTopicsResult
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.header.Header

import java.util.Properties
import java.util.concurrent.{ CountDownLatch, TimeUnit }

object Implicits {
  import scala.language.implicitConversions

  implicit def mapToProperties(map: Map[String, AnyRef]): Properties = {
    val props = new Properties()
    map.foreach { case (key, value) => props.put(key, value) }
    props
  }

  implicit class RichCreateTopicsResult(r: CreateTopicsResult) {

    def whenTopicsReady[T](f: => T): T = {
      val latch = new CountDownLatch(1)
      r.all().whenComplete((_: Void, b: Throwable) => {
        latch.countDown()
        if (b != null) throw b
      })
      latch.await(30, TimeUnit.SECONDS)
      f
    }
  }

  /**
   * Constructor helper for [[ProducerRecord]] class
   */
  def ProducerRecord[K, V](
    topic: String,
    key: K,
    value: V,
    headers: Iterable[Header]): ProducerRecord[K, V] = {
    import scala.jdk.CollectionConverters._
    new ProducerRecord[K, V](
      topic,
      null,
      key,
      value,
      headers.asJava)
  }
}
