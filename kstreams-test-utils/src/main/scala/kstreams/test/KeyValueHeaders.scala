package kstreams.test

import org.apache.kafka.common.header.Headers
import org.apache.kafka.streams.KeyValue

case class KeyValueHeaders[K, V](k: K, v: V, headers: Headers) extends KeyValue[K, V](k, v)
