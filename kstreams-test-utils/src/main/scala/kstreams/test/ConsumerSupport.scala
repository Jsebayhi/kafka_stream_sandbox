package kstreams.test

import org.apache.kafka.clients.consumer.{ ConsumerConfig, ConsumerRecords, KafkaConsumer }
import org.apache.kafka.common.serialization.Deserializer

import java.time.Duration
import java.util.Collections
import scala.collection.mutable.ListBuffer
import scala.jdk.CollectionConverters.IterableHasAsScala
import org.scalatest.concurrent.{ Eventually, IntegrationPatience }
import org.scalatest.exceptions.TestFailedDueToTimeoutException

/**
 * This object gives helper methods to consumes messages from a Kafka topic. It uses configuration from [[KafkaEnvConf]] object.
 */
object ConsumerSupport extends Eventually with IntegrationPatience {
  import Implicits._
  def consumerConf(id: String = Utils.randomTimeID): Map[String, AnyRef] = {
    Map(
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> KafkaEnvConf.kafka.url,
      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "earliest",
      ConsumerConfig.CLIENT_ID_CONFIG -> s"client-$id",
      ConsumerConfig.GROUP_ID_CONFIG -> s"group-$id"
    )
  }

  def assertEmptyTopic[K, V](
    topic: String,
    keyDeserializer: Deserializer[K],
    valueDeserializer: Deserializer[V]): Unit = {
    val consumer = new KafkaConsumer[K, V](consumerConf(), keyDeserializer, valueDeserializer)
    consumer.subscribe(Collections.singleton(topic))
    assertEmptyTopic(consumer)
  }

  def assertEmptyTopic[K, V](consumer: KafkaConsumer[K, V]): Unit = {
    try {
      val content = consumeRecords(consumer, 1)
      assert(content.isEmpty)
    } catch {
      case e: TestFailedDueToTimeoutException if e.getCause.isInstanceOf[NotEnoughMessageInTheTopic] =>
    }
  }

  def consumeRecords[K, V](
    topic: String,
    expectedMinMessagesCount: Int,
    keyDeserializer: Deserializer[K],
    valueDeserializer: Deserializer[V]): List[KeyValueHeaders[K, V]] = {
    val consumer = new KafkaConsumer[K, V](consumerConf(), keyDeserializer, valueDeserializer)
    try {
      consumer.subscribe(Collections.singleton(topic))
      consumeRecords(consumer, expectedMinMessagesCount)
    } finally consumer.close()
  }

  def consumeRecords[K, V](
    consumer: KafkaConsumer[K, V],
    expectedMinMessagesCount: Int): List[KeyValueHeaders[K, V]] = {
    val consumedMessages = ListBuffer.empty[KeyValueHeaders[K, V]]
    eventually {
      val records: ConsumerRecords[K, V] = consumer.poll(Duration.ofMillis(100))
      records.asScala.foreach { rec =>
        consumedMessages += new KeyValueHeaders[K, V](rec.key(), rec.value(), rec.headers())
      }
      if (consumedMessages.size < expectedMinMessagesCount) {
        throw NotEnoughMessageInTheTopic(expectedMinMessagesCount, consumedMessages.size)
      }
    }
    consumedMessages.toList
  }
}

case class NotEnoughMessageInTheTopic(expected: Int, got: Int) extends RuntimeException(s"expected $expected messages but found $got")