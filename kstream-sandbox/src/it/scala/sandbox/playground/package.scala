package sandbox

import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig
import org.apache.kafka.common.serialization.{ Serde, Serdes }
import org.apache.kafka.streams.kstream.Joined
import org.apache.kafka.streams.scala.kstream.{ Consumed, Grouped, Produced, StreamJoined }

package object playground {
  import scala.jdk.CollectionConverters._
  val defaultSerdeProperties = Map(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG -> "useless").asJava

  val stringKeySerde: Serde[String] = Serdes.String()
  stringKeySerde.configure(defaultSerdeProperties, true)
  val stringValueSerde: Serde[String] = Serdes.String()
  stringValueSerde.configure(defaultSerdeProperties, false)
  val KStringVStringConsumed: Consumed[String, String] = {
    Consumed.`with`(stringKeySerde, stringValueSerde)
  }
  val KStringVStringProduced: Produced[String, String] = {
    Produced.`with`(stringKeySerde, stringValueSerde)
  }
  val stringsJoined: Joined[String, String, String] = {
    Joined.`with`(stringKeySerde, stringValueSerde, stringValueSerde)
  }
  val stringStreamJoin: StreamJoined[String, String, String] = {
    StreamJoined.`with`[String, String, String](stringKeySerde, stringValueSerde, stringValueSerde)
  }
  val stringGrouped: Grouped[String, String] = {
    Grouped.`with`(stringKeySerde, stringValueSerde)
  }
}
