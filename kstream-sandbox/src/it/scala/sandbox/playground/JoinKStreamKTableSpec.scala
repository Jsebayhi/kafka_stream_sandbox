package sandbox.playground

import kstreams.test.Implicits.{ RichCreateTopicsResult, mapToProperties }
import kstreams.test.{ KStreamTestSupport, PipelineTestManager, Utils }
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.{ KafkaStreams, KeyValue, StreamsConfig }
import org.scalatest.GivenWhenThen
import org.scalatest.concurrent.{ Eventually, IntegrationPatience }
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import org.slf4j.LoggerFactory

import java.nio.file.Files
import java.time.Instant

class JoinKStreamKTableSpec
  extends AnyFunSpec
  with Matchers
  with GivenWhenThen
  with Eventually
  with IntegrationPatience
  with KStreamTestSupport {

  private val logger = LoggerFactory.getLogger(this.getClass)

  describe("kstream to ktable join") {

    it("should provide a result if an event is pushed in the stream after the referential as been populated") {
      testTopology {
        case (conf) =>
          When("sending a message to the ktable")
          producer.produceRecords[String, String](
            topic = conf.inputKtableTopic,
            records = Seq(new KeyValue("key", "toto")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )
          And("sending a message to the stream")
          producer.produceRecords[String, String](
            topic = conf.inputStreamTopic,
            records = Seq(new KeyValue("key", "bigger ")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Then("expecting a produced result")
          val output: List[KeyValue[String, String]] = consumer.consumeRecords[String, String](
            topic = conf.output,
            keyDeserializer = stringKeySerde.deserializer(),
            valueDeserializer = stringValueSerde.deserializer(),
            expectedMinMessagesCount = 1
          )

          output should contain theSameElementsAs (List(new KeyValue("key", "bigger toto")))
      }
    }

    it("should provide no result if an event is pushed in the stream before the referential as been populated") {
      testTopology {
        case (conf) =>
          When("sending a message to the stream")
          producer.produceRecords[String, String](
            topic = conf.inputStreamTopic,
            records = Seq(new KeyValue("key", "bigger ")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Thread.sleep(300)

          And("sending a message to the ktable")
          producer.produceRecords[String, String](
            topic = conf.inputKtableTopic,
            records = Seq(new KeyValue("key", "toto")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Then("expecting no produced result")
          consumer.assertEmptyTopic(
            topic = conf.output,
            keyDeserializer = stringKeySerde.deserializer(),
            valueDeserializer = stringValueSerde.deserializer()
          )
      }
    }

    it("should provide a result if an event update is pushed in the stream after the referential as been populated") {
      testTopology {
        case (conf) =>
          When("sending a message to the stream")
          producer.produceRecords[String, String](
            topic = conf.inputStreamTopic,
            records = Seq(new KeyValue("key", "bigger ")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Thread.sleep(300)

          And("sending a message to the ktable")
          producer.produceRecords[String, String](
            topic = conf.inputKtableTopic,
            records = Seq(new KeyValue("key", "toto")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Then("expecting no produced result")
          consumer.assertEmptyTopic(
            topic = conf.output,
            keyDeserializer = stringKeySerde.deserializer(),
            valueDeserializer = stringValueSerde.deserializer()
          )

          When("sending a message to the stream")
          producer.produceRecords[String, String](
            topic = conf.inputStreamTopic,
            records = Seq(new KeyValue("key", "updated ")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Then("expecting no more than one produced result")
          val output: List[KeyValue[String, String]] = consumer.consumeRecords[String, String](
            topic = conf.output,
            keyDeserializer = stringKeySerde.deserializer(),
            valueDeserializer = stringValueSerde.deserializer(),
            expectedMinMessagesCount = 1
          )

          output should contain theSameElementsAs (List(new KeyValue("key", "updated toto")))
      }
    }

    it("should provide no result if an event is pushed in the stream after the referential as been populated but with an older timestamp") {
      testTopology {
        case (conf) =>
          val now = Instant.now()
          val before = now.minusMillis(java.time.Duration.ofDays(30).toMillis)

          When(s"populating the ktable with a data at timestamp ${now.toEpochMilli} ($now)")
          producer.produceRecords[String, String](
            records = Seq(new ProducerRecord[String, String](conf.inputKtableTopic, null, now.toEpochMilli, "key", "titi")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Then(s"sending an old message to the stream with a timestamp ${before.toEpochMilli} ($before)")
          producer.produceRecords[String, String](
            records = Seq(new ProducerRecord[String, String](conf.inputStreamTopic, null, before.toEpochMilli, "key", "toto")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Then("expecting a result")
          val res = consumer.consumeRecords[String, String](
            topic = conf.output,
            keyDeserializer = stringKeySerde.deserializer(),
            valueDeserializer = stringValueSerde.deserializer(),
            expectedMinMessagesCount = 1
          )
          res.toList should contain theSameElementsAs (List(new KeyValue("key", "tototiti")))
      }
    }
  }

  def buildTopology(conf: KTableKTableJoinTestConf): StreamsBuilder = {
    val streamsBuilder = new StreamsBuilder()
    val ktableInput = streamsBuilder.stream(conf.inputKtableTopic)(KStringVStringConsumed)
    val streamInput = streamsBuilder.stream(conf.inputStreamTopic)(KStringVStringConsumed)

    streamInput.join[String, String](ktableInput.toTable)((left, right) => {
      logger.info(s"viewing $left and $right")
      left + right
    })(stringsJoined)
      .to(conf.output)(KStringVStringProduced)

    if (logger.isDebugEnabled) {
      ktableInput.peek((_, d) => logger.debug(s"ktableInput: viewing $d"))
      streamInput.peek((_, d) => logger.debug(s"streamInput: viewing $d"))
    }

    streamsBuilder
  }

  object KTableKTableJoinTestConf {
    def apply(testId: String): KTableKTableJoinTestConf = KTableKTableJoinTestConf(
      inputKtableTopic = s"inputTopic-$testId",
      inputStreamTopic = s"input2Topic-$testId",
      output = s"outputTopic-$testId"
    )
  }
  case class KTableKTableJoinTestConf(
      inputKtableTopic: String,
      inputStreamTopic: String,
      output: String
  ) {
    def listTopics = List(inputKtableTopic, inputStreamTopic, output)
  }

  private def testTopology(f: KTableKTableJoinTestConf => Unit): Unit = {
    val testId = Utils.randomTimeID

    info(s"test id: $testId")
    val stateStoreTempDir = Files.createTempDirectory(s"state-$testId").toString
    val kafkaSettings: Map[String, AnyRef] = Map(
      StreamsConfig.APPLICATION_ID_CONFIG -> s"test-$testId",
      StreamsConfig.BOOTSTRAP_SERVERS_CONFIG -> kafkaEnvConf.kafka.url,
      StreamsConfig.STATE_DIR_CONFIG -> stateStoreTempDir,
      StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG -> Int.box(0),
      StreamsConfig.APPLICATION_SERVER_CONFIG -> "localhost:8080",

      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "earliest",
    )

    /*
    val schemaRegistryUrl = "http://" + KafkaEnvConf.schemaRegistry.url

    import scala.jdk.CollectionConverters._
    val schemaRegistryClient = new CachedSchemaRegistryClient(List(schemaRegistryUrl).asJava, 1000)
    */

    val conf = KTableKTableJoinTestConf(testId)

    val topology = buildTopology(conf).build(kafkaSettings)
    logger.debug(s"topology: ${topology.describe()}")
    adminClient.doAndClose { adminClient =>
      import scala.jdk.CollectionConverters._
      adminClient.createTopics(conf.listTopics.map(n => new NewTopic(n, 1, 1.toShort)).asJava)
    }.whenTopicsReady {
      val stream = new KafkaStreams(topology, mapToProperties(kafkaSettings))
      val pipeline = new PipelineTestManager(stream)
      pipeline.start()

      try {
        eventually {
          require(pipeline.isReady, "stream state not RUNNING")
        }
        f(conf)
      } finally pipeline.close()
    }
  }
}
