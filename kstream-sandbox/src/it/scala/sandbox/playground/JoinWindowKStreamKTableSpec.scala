package sandbox.playground

import kstreams.test.Implicits.{ RichCreateTopicsResult, mapToProperties }
import kstreams.test.{ KStreamTestSupport, PipelineTestManager, Utils }
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.streams.kstream.JoinWindows
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.{ KafkaStreams, KeyValue, StreamsConfig }
import org.scalatest.GivenWhenThen
import org.scalatest.concurrent.{ Eventually, IntegrationPatience }
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import org.slf4j.LoggerFactory

import java.nio.file.Files
import java.time.{ Duration, Instant }

class JoinWindowKStreamKTableSpec
  extends AnyFunSpec
  with Matchers
  with GivenWhenThen
  with Eventually
  with IntegrationPatience
  with KStreamTestSupport {

  private val logger = LoggerFactory.getLogger(this.getClass)

  describe("kstream to ktable join") {

    it("should provide a result if an event on the right and on the left arrive before the window expire") {
      testTopology {
        case (conf) =>
          When("sending a message to the ktable")
          producer.produceRecords[String, String](
            topic = conf.inputTopic,
            records = Seq(new KeyValue("key", "bigger ")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )
          And("sending a message to the stream")
          producer.produceRecords[String, String](
            topic = conf.input2Topic,
            records = Seq(new KeyValue("key", "toto")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Then("expecting a produced result")
          val output: List[KeyValue[String, String]] = consumer.consumeRecords[String, String](
            topic = conf.output,
            keyDeserializer = stringKeySerde.deserializer(),
            valueDeserializer = stringValueSerde.deserializer(),
            expectedMinMessagesCount = 1
          )

          output should contain theSameElementsAs (List(new KeyValue("key", "bigger toto")))
      }
    }

    it("should provide no result if an event is pushed pn the right too long after the left") {
      testTopology {
        case (conf) =>
          val now = Instant.now()
          val before = now.minusSeconds(4)

          When(s"sending message to the right with a data at timestamp ${before.toEpochMilli}")
          producer.produceRecords[String, String](
            records = Seq(new ProducerRecord[String, String](conf.inputTopic, null, before.toEpochMilli, "key", "bigger ")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )
          And(s"sending message to the right with a timestamp ${now.toEpochMilli}")
          producer.produceRecords[String, String](
            records = Seq(new ProducerRecord[String, String](conf.input2Topic, null, now.toEpochMilli, "key", "toto")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Then("expecting no produced result")
          consumer.assertEmptyTopic(
            topic = conf.output,
            keyDeserializer = stringKeySerde.deserializer(),
            valueDeserializer = stringValueSerde.deserializer()
          )
      }
    }

    it("should provide no result if an event is pushed to the left too long after the right") {
      testTopology {
        case (conf) =>
          val now = Instant.now()
          val before = now.minusSeconds(4)

          When(s"sending message to the right with a timestamp ${before.toEpochMilli}")
          producer.produceRecords[String, String](
            records = Seq(new ProducerRecord[String, String](conf.input2Topic, null, before.toEpochMilli, "key", "toto")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          And(s"sending message to the right with a data at timestamp ${now.toEpochMilli}")
          producer.produceRecords[String, String](
            records = Seq(new ProducerRecord[String, String](conf.inputTopic, null, now.toEpochMilli, "key", "bigger ")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Then("expecting no produced result")
          consumer.assertEmptyTopic(
            topic = conf.output,
            keyDeserializer = stringKeySerde.deserializer(),
            valueDeserializer = stringValueSerde.deserializer()
          )
      }
    }
  }

  def buildTopology(conf: KTableKTableJoinTestConf): StreamsBuilder = {
    val streamsBuilder = new StreamsBuilder()
    val in1 = streamsBuilder.stream(conf.inputTopic)(KStringVStringConsumed)
    val in2 = streamsBuilder.stream(conf.input2Topic)(KStringVStringConsumed)

    in1.join[String, String](in2)(
      (left, right) => {
        logger.info(s"viewing $left and $right")
        left + right
      }, JoinWindows.of(Duration.ofSeconds(1)))(stringStreamJoin.withName("stream_join"))
      .to(conf.output)(KStringVStringProduced)

    if (logger.isDebugEnabled) {
      in1.peek((_, d) => logger.debug(s"viewing $d"))
      in2.peek((_, d) => logger.debug(s"viewing $d"))
    }

    streamsBuilder
  }

  object KTableKTableJoinTestConf {
    def apply(testId: String): KTableKTableJoinTestConf = KTableKTableJoinTestConf(
      inputTopic = s"inputTopic-$testId",
      input2Topic = s"input2Topic-$testId",
      output = s"outputTopic-$testId"
    )
  }
  case class KTableKTableJoinTestConf(
      inputTopic: String,
      input2Topic: String,
      output: String
  ) {
    def listTopics = List(inputTopic, input2Topic, output)
  }

  private def testTopology(f: KTableKTableJoinTestConf => Unit): Unit = {
    val testId = Utils.randomTimeID

    info(s"test id: $testId")
    val stateStoreTempDir = Files.createTempDirectory(s"state-$testId").toString
    val kafkaSettings: Map[String, AnyRef] = Map(
      StreamsConfig.APPLICATION_ID_CONFIG -> s"test-$testId",
      StreamsConfig.BOOTSTRAP_SERVERS_CONFIG -> kafkaEnvConf.kafka.url,
      StreamsConfig.STATE_DIR_CONFIG -> stateStoreTempDir,
      StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG -> Int.box(0),
      StreamsConfig.APPLICATION_SERVER_CONFIG -> "localhost:8080",

      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "earliest",
    )

    /*
    val schemaRegistryUrl = "http://" + KafkaEnvConf.schemaRegistry.url

    import scala.jdk.CollectionConverters._
    val schemaRegistryClient = new CachedSchemaRegistryClient(List(schemaRegistryUrl).asJava, 1000)
    */

    val conf = KTableKTableJoinTestConf(testId)

    val topology = buildTopology(conf).build(kafkaSettings)
    logger.debug(s"topology: ${topology.describe()}")
    adminClient.doAndClose { adminClient =>
      import scala.jdk.CollectionConverters._
      adminClient.createTopics(conf.listTopics.map(n => new NewTopic(n, 1, 1.toShort)).asJava)
    }.whenTopicsReady {
      val stream = new KafkaStreams(topology, mapToProperties(kafkaSettings))
      val pipeline = new PipelineTestManager(stream)
      pipeline.start()

      try {
        eventually {
          require(pipeline.isReady, "stream state not RUNNING")
        }
        f(conf)
      } finally pipeline.close()
    }
  }
}
