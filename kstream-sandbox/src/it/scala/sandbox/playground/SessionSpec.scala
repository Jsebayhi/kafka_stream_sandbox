package sandbox.playground

import kstreams.test.Implicits.{ RichCreateTopicsResult, mapToProperties }
import kstreams.test.{ ConsumerSupport, KStreamTestSupport, PipelineTestManager, Utils }
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.clients.consumer.{ ConsumerConfig, KafkaConsumer }
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.streams.kstream.SessionWindows
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.scala.kstream.{ KStream, Materialized }
import org.apache.kafka.streams.{ KafkaStreams, KeyValue, StreamsConfig }
import org.scalatest.GivenWhenThen
import org.scalatest.concurrent.{ Eventually, IntegrationPatience }
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import org.slf4j.LoggerFactory

import java.nio.file.Files
import java.time.{ Duration, Instant }
import java.util.Collections

class SessionSpec
  extends AnyFunSpec
  with Matchers
  with GivenWhenThen
  with Eventually
  with IntegrationPatience
  with KStreamTestSupport {

  private val logger = LoggerFactory.getLogger(this.getClass)

  describe("session") {
    it("should behave") {
      testTopology {
        case (conf) =>
          val kconsumer = new KafkaConsumer[String, String](ConsumerSupport.consumerConf(), stringKeySerde.deserializer(), stringKeySerde.deserializer())
          kconsumer.subscribe(Collections.singleton(conf.output))

          val now = Instant.now()
          val before = now.atZone(java.time.ZoneId.systemDefault()).minusHours(1).toInstant
          When("sending a first message")
          producer.produceRecords[String, String](
            records = Seq(new ProducerRecord[String, String](conf.inputTopic, null, before.toEpochMilli, "key", "toto")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Then("expecting a produced result")
          val output: List[KeyValue[String, String]] = consumer.consumeRecords[String, String](
            kconsumer,
            expectedMinMessagesCount = 1
          )
          output should contain theSameElementsAs (List(new KeyValue("key", "toto")))

          And("sending another message")
          producer.produceRecords[String, String](
            records = Seq(new ProducerRecord[String, String](conf.inputTopic, null, now.toEpochMilli, "key", "titi")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          And("sending another message")
          producer.produceRecords[String, String](
            records = Seq(new ProducerRecord[String, String](conf.inputTopic, null, now.toEpochMilli, "key", "tata")),
            keySerializer = stringKeySerde.serializer(),
            valueSerializer = stringValueSerde.serializer()
          )

          Then("expecting a produced result")
          val output2: List[KeyValue[String, String]] = consumer.consumeRecords[String, String](
            kconsumer,
            expectedMinMessagesCount = 1
          )

          output2.map(_.value) should contain theSameElementsInOrderAs (List(
            new KeyValue(key, null),
            new KeyValue(key, "tototiti"),
            new KeyValue("key", null),
            new KeyValue("key", "tototititata")
          ).map(_.value)
          )
      }
    }
  }

  def buildTopology(conf: KTableKTableJoinTestConf): StreamsBuilder = {
    val streamsBuilder = new StreamsBuilder()
    val in1 = streamsBuilder.stream(conf.inputTopic)(KStringVStringConsumed)

    val res: KStream[String, String] = in1.groupByKey(stringGrouped)
      .windowedBy(SessionWindows.`with`(Duration.ofHours(2)))
      .reduce((first, second) => first + second)(Materialized.`with`(stringKeySerde, stringValueSerde))
      .toStream
      .selectKey((w, _) => w.key())

    res.to(conf.output)(KStringVStringProduced)

    if (logger.isDebugEnabled) {
      in1.peek((_, d) => logger.debug(s"viewing input: $d"))
      res.peek((_, d) => logger.debug(s"viewing result: $d"))
    }

    streamsBuilder
  }

  object KTableKTableJoinTestConf {
    def apply(testId: String): KTableKTableJoinTestConf = KTableKTableJoinTestConf(
      inputTopic = s"inputTopic-$testId",
      output = s"outputTopic-$testId"
    )
  }
  case class KTableKTableJoinTestConf(
      inputTopic: String,
      output: String
  ) {
    def listTopics = List(inputTopic, output)
  }

  private def testTopology(f: KTableKTableJoinTestConf => Unit): Unit = {
    val testId = Utils.randomTimeID

    info(s"test id: $testId")
    val stateStoreTempDir = Files.createTempDirectory(s"state-$testId").toString
    val kafkaSettings: Map[String, AnyRef] = Map(
      StreamsConfig.APPLICATION_ID_CONFIG -> s"test-$testId",
      StreamsConfig.BOOTSTRAP_SERVERS_CONFIG -> kafkaEnvConf.kafka.url,
      StreamsConfig.STATE_DIR_CONFIG -> stateStoreTempDir,
      StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG -> Int.box(0),
      StreamsConfig.APPLICATION_SERVER_CONFIG -> "localhost:8080",
      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "earliest",
    )

    /*
    val schemaRegistryUrl = "http://" + KafkaEnvConf.schemaRegistry.url

    import scala.jdk.CollectionConverters._
    val schemaRegistryClient = new CachedSchemaRegistryClient(List(schemaRegistryUrl).asJava, 1000)
    */

    val conf = KTableKTableJoinTestConf(testId)

    val topology = buildTopology(conf).build(kafkaSettings)
    logger.debug(s"topology: ${topology.describe()}")
    adminClient.doAndClose { adminClient =>
      import scala.jdk.CollectionConverters._
      adminClient.createTopics(conf.listTopics.map(n => new NewTopic(n, 1, 1.toShort)).asJava)
    }.whenTopicsReady {
      val stream = new KafkaStreams(topology, mapToProperties(kafkaSettings))
      val pipeline = new PipelineTestManager(stream)
      pipeline.start()

      try {
        eventually {
          require(pipeline.isReady, "stream state not RUNNING")
        }
        f(conf)
      } finally pipeline.close()
    }
  }
}
