import Dependencies._
import sbt._
import sbt.Keys._

object KStreamsSandbox {
  private val Dependencies: Seq[sbt.librarymanagement.ModuleID] = {
      KafkaStream ++
      PureConfig ++
      Slf4jUsingLogback.Dependencies() ++
      ScalaTest("compile") ++ Seq(
        KafkaStreamsTest % "test,it"
      )

  }

  private val Overrides: Seq[sbt.librarymanagement.ModuleID] = {
    Slf4jUsingLogback.overrides()
  }

  private val Exclusions: Seq[ExclusionRule] = {
    Slf4jUsingLogback.ExclusionRules
  }
//: Seq[Def.Setting[Seq[ModuleID]]]
  val settings = {
    Seq(
      libraryDependencies ++= Dependencies,
      dependencyOverrides ++= Overrides,
      excludeDependencies ++= Exclusions
    )
  }
}
