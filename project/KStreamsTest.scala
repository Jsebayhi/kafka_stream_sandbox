import Dependencies._
import sbt._
import sbt.Keys._

object KStreamsTest {
  val Dependencies: Seq[sbt.librarymanagement.ModuleID] = {
      KafkaStream ++
      PureConfig ++
      Slf4jUsingLogback.Dependencies() ++
      ScalaTest("compile") ++ Seq(
        KafkaStreamsTest % "test,it"
      )

  }

  val Overrides: Seq[sbt.librarymanagement.ModuleID] = {
    Slf4jUsingLogback.overrides()
  }

  val Exclusions: Seq[ExclusionRule] = {
    Slf4jUsingLogback.ExclusionRules
  }

  val settings = {
    Seq(
      libraryDependencies ++= Dependencies,
      dependencyOverrides ++= Overrides,
      excludeDependencies ++= Exclusions
    )
  }
}
