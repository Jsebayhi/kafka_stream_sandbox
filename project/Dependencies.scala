import sbt._

object Dependencies {

  private val ScalaTestVersion = "3.2.0"
  private val KafkaClientVersion = "2.8.0"

  lazy val TypeSafeConfig = Seq("com.typesafe" % "config" % "1.4.0")

  lazy val PureConfigVersion = "0.14.0"
  lazy val PureConfig = TypeSafeConfig ++ Seq(
    "com.chuusai" %% "shapeless" % "2.3.3",
    "com.github.pureconfig" %% "pureconfig" % PureConfigVersion,
    "com.github.pureconfig" %% "pureconfig-core" % PureConfigVersion,
    "com.github.pureconfig" %% "pureconfig-generic" % PureConfigVersion
  )

  val Avro = "org.apache.avro" % "avro" % "1.10.0"

  lazy val KafkaStream = {
    //probably useless, overriden by kafka-streams-avro-serde kafka client dep
    val KafkaClients = "org.apache.kafka" % "kafka-clients" % KafkaClientVersion
    val KStreams = "org.apache.kafka" % "kafka-streams" % KafkaClientVersion
    val KafkaStreamsScala = "org.apache.kafka" %% "kafka-streams-scala" % KafkaClientVersion
    // can import a conflicting kafka client version.
    val ConfluentKStreamAvroSerde = "io.confluent" % "kafka-streams-avro-serde" % "6.2.0"

    Seq(KafkaClients, KStreams, KafkaStreamsScala, ConfluentKStreamAvroSerde)
  }

  lazy val KafkaStreamsTest = "org.apache.kafka" % "kafka-streams-test-utils" % KafkaClientVersion

  def ScalaTest(scope: String) = {
    Seq(
      "org.scalatest" %% "scalatest" % ScalaTestVersion,
      "org.scalatest" %% "scalatest-flatspec" % ScalaTestVersion,
      "org.scalatest" %% "scalatest-matchers-core" % ScalaTestVersion
      ).map(_ % scope)
  }
}
