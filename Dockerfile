FROM hseeberger/scala-sbt:8u222_1.3.5_2.13.1

ARG SPE_USER_ID=1000
ARG SPE_GROUP_ID=1000
ARG USER_NAME=mrbouh

ENV SPE_USER_ID ${SPE_USER_ID}
ENV SPE_GROUP_ID ${SPE_GROUP_ID}
ENV USER_NAME ${USER_NAME}

RUN groupadd --non-unique -g ${SPE_GROUP_ID} ${USER_NAME}
RUN useradd -g ${SPE_GROUP_ID} -u ${SPE_USER_ID} -k /root -m ${USER_NAME}


CMD "/bin/bash"