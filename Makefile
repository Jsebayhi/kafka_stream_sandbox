
SHELL = /bin/bash

.DEFAULT_GOAL := help

all: sbtAll down

up: ## start all docker
	@docker-compose up -d --build

down: ## stop all docker
	@docker-compose down

diveIn: up ## start all docker the connect to the build docker
	@docker-compose exec --user $(USER) --privileged build bash

clean: up
	@docker-compose exec --user $(USER) --privileged build sbt 'clean'

compile: up
	@docker-compose exec --user $(USER) --privileged build sbt 'compile'

runTests: up
	@docker-compose exec --user $(USER) --privileged build sbt 'test:test'

runIoTest: up ## start all docker then run the it test
	@docker-compose exec --user $(USER) --privileged build sbt 'it:test'

sbtAll: up
	@docker-compose exec --user $(USER) --privileged build sbt 'clean compile test it:test'

.PHONY: help
# rules' name with length over 25 are truncated.
# credit: https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## show available rules with their description if exist
	@grep -h -E '^[0-9a-zA-Z_-]+:' $(MAKEFILE_LIST) | grep -v "### not shown" | awk 'BEGIN {FS = ":(.*?## )?"}; {printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}'