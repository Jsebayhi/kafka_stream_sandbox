import sbt._
import com.typesafe.sbt.SbtScalariform.autoImport.scalariformPreferences
import scalariform.formatter.preferences.{AlignSingleLineCaseStatements, DanglingCloseParenthesis, DoubleIndentConstructorArguments, Preserve}

import sbt.Keys._

lazy val root = (project in file("."))
  .settings(
    name := "kafka-stream-sandbox",
    Global / cancelable := true, // Lets us CTRL-C partest without exiting SBT entirely
    Global / onChangedBuildSource := ReloadOnSourceChanges,// sbt auto reload if build.sbt changed.
    updateOptions := updateOptions.value.withLatestSnapshots(true),
    inThisBuild(Seq(
      organization := "sebayhi.jeremy.sandbox",
      scalaVersion := "2.13.1",
      IntegrationTest / fork := false,
      IntegrationTest / testForkedParallel := false,
      IntegrationTest / parallelExecution  := false,
      Test / fork := true,
      Test / testForkedParallel := true,
      Test / parallelExecution := true,
      concurrentRestrictions := Seq(Tags.limitAll(1)), //set to 1 to avoid that avsc schema are compiled before they are downloaded
      scalariformPreferences := scalariformPreferences.value
          .setPreference(AlignSingleLineCaseStatements, true)
          .setPreference(DoubleIndentConstructorArguments, true)
          .setPreference(DanglingCloseParenthesis, Preserve),
      scalacOptions ++= Seq(
        "-deprecation",
        "-encoding",
        "UTF-8",
        "-feature",
        "-explaintypes",
        "-language:existentials",
        "-Xfatal-warnings",
        "-Xlint"
      )
    ))
  ).aggregate(
  kStreamSandbox,
  kStreamsTest
)

lazy val kStreamSandboxName = "kstream-sandbox"
lazy val kStreamSandbox = (project in file(kStreamSandboxName))
  .withId(kStreamSandboxName)
  .dependsOn(kStreamsTest)
  .configs(IntegrationTest)
  .settings(
    name := kStreamSandboxName,
    Defaults.itSettings,
    scalariformItSettings,
    KStreamsSandbox.settings
  )


lazy val kStreamsTestName = "kstreams-test-utils"
lazy val kStreamsTest = (project in file(kStreamsTestName))
  .withId(kStreamsTestName)
  .configs(IntegrationTest)
  .settings(
    name := kStreamsTestName,
    Defaults.itSettings,
    scalariformItSettings,
    KStreamsTest.settings
  )